package com.wedoogift.backend.service;

import com.wedoogift.backend.exception.UserNotFoundException;
import com.wedoogift.backend.model.entity.Gift;
import com.wedoogift.backend.model.entity.User;
import com.wedoogift.backend.model.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.function.Predicate;

@Service
@RequiredArgsConstructor
public class UserServiceDefault implements UserService {

    private final UserRepository userRepository;

    @Override
    public BigDecimal getUserBalance(String userEmail) throws UserNotFoundException {
        User user = findUser(userEmail);
        return user.getGifts().stream()
                .filter(Predicate.not(this::isExpiredGift))
                .map(Gift::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private boolean isExpiredGift(Gift gift) {
        return gift.getExpiredDate().isBefore(LocalDate.now());
    }

    @Override
    public User findUser(String userEmail) throws UserNotFoundException {
        return userRepository.findByEmail(userEmail)
                .orElseThrow(() -> new UserNotFoundException(userEmail));
    }

}
