package com.wedoogift.backend.controller;

import com.wedoogift.backend.exception.UserNotFoundException;
import com.wedoogift.backend.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@Tag(name = "User API")
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;

    @Operation(summary = "Récupère le solde d'un utilisateur via une adresse mail")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200", description = "Solde récupéré",
                            content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)
                    ),
                    @ApiResponse(
                            responseCode = "404", description = "Utilisateur inconnu",
                            content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)
                    ),
                    @ApiResponse(
                            responseCode = "500", description = "Erreur interne",
                            content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)
                    )
            }
    )
    @GetMapping("/{userEmail}/balance")
    public ResponseEntity<String> getBalance(
            @PathVariable @Parameter(description = "Adresse mail de l'utilisateur") String userEmail) {
        ResponseEntity<String> response;
        try {
            BigDecimal userBalance = userService.getUserBalance(userEmail);
            response = ResponseEntity.ok(formatUserBalance(userBalance));
        } catch (UserNotFoundException e) {
            response = ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        } catch (Exception e) {
            logger.error("Impossible de récupérer la balance de l'utilisateur '{}'", userEmail, e);
            response = ResponseEntity.internalServerError().build();
        }
        return response;
    }

    private String formatUserBalance(BigDecimal userBalance) {
        return "$" + userBalance.stripTrailingZeros().toPlainString();
    }

}
