package com.wedoogift.backend.controller;

import com.wedoogift.backend.exception.CompanyNotFoundException;
import com.wedoogift.backend.exception.TooExpensiveGiftException;
import com.wedoogift.backend.exception.UserNotFoundException;
import com.wedoogift.backend.model.dto.GiftDto;
import com.wedoogift.backend.model.entity.GiftType;
import com.wedoogift.backend.service.GiftService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "Gift API")
@RequestMapping("/gift")
@RequiredArgsConstructor
public class GiftController {

    private static final Logger logger = LoggerFactory.getLogger(GiftController.class);

    private final GiftService giftService;

    @Operation(summary = "Envoie une carte cadeau d'une entreprise à un utilisateur")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204", description = "Cadeau envoyé",
                            content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)
                    ),
                    @ApiResponse(
                            responseCode = "404", description = "Entreprise ou utilisateur inconnu",
                            content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)
                    ),
                    @ApiResponse(
                            responseCode = "406", description = "Solde insuffisant",
                            content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)
                    ),
                    @ApiResponse(
                            responseCode = "500", description = "Erreur interne",
                            content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)
                    )
            }
    )
    @PostMapping("/card")
    public ResponseEntity<String> sendCardGift(@RequestBody GiftDto giftDto) {
        return sendGift(giftDto, GiftType.CARD);
    }

    @Operation(summary = "Envoie un ticket restaurant d'une entreprise à un utilisateur")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "204", description = "Cadeau envoyé",
                            content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)
                    ),
                    @ApiResponse(
                            responseCode = "404", description = "Entreprise ou utilisateur inconnu",
                            content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)
                    ),
                    @ApiResponse(
                            responseCode = "406", description = "Solde insuffisant",
                            content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)
                    ),
                    @ApiResponse(
                            responseCode = "500", description = "Erreur interne",
                            content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)
                    )
            }
    )
    @PostMapping("/meal")
    public ResponseEntity<String> sendMealGift(@RequestBody GiftDto giftDto) {
        return sendGift(giftDto, GiftType.MEAL);
    }

    private ResponseEntity<String> sendGift(GiftDto giftDto, GiftType giftType) {
        ResponseEntity<String> response;
        try {
            giftService.sendGift(giftDto, giftType);
            response = ResponseEntity.noContent().build();
        } catch (UserNotFoundException | CompanyNotFoundException e) {
            response = ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        } catch (TooExpensiveGiftException e) {
            response = ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        } catch (Exception e) {
            logger.error("Impossible d'envoyer le cadeau {}", giftDto, e);
            response = ResponseEntity.internalServerError().build();
        }
        return response;
    }

}
