package com.wedoogift.backend.service;

import com.wedoogift.backend.exception.CompanyNotFoundException;
import com.wedoogift.backend.exception.TooExpensiveGiftException;
import com.wedoogift.backend.exception.UserNotFoundException;
import com.wedoogift.backend.model.dto.GiftDto;
import com.wedoogift.backend.model.entity.Company;
import com.wedoogift.backend.model.entity.Gift;
import com.wedoogift.backend.model.entity.GiftType;
import com.wedoogift.backend.model.entity.User;
import com.wedoogift.backend.model.repository.GiftRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.util.Objects;

@Service
public class GiftServiceDefault implements GiftService {

    private final UserService userService;
    private final CompanyService companyService;
    private final GiftRepository giftRepository;
    private final int giftCardExpiresInDays;

    public GiftServiceDefault(
            @Value("${wedoogift.gift.card.expires-in-days}") int giftCardExpiresInDays,
            UserService userService,
            CompanyService companyService,
            GiftRepository giftRepository) {
        this.giftCardExpiresInDays = giftCardExpiresInDays;
        this.userService = userService;
        this.companyService = companyService;
        this.giftRepository = giftRepository;
    }

    @Override
    @Transactional
    public void sendGift(GiftDto giftDto, GiftType giftType) throws CompanyNotFoundException, UserNotFoundException,
            TooExpensiveGiftException {
        requireGiftType(giftType);

        Company company = companyService.findCompany(giftDto.companyName());
        if (!hasEnoughMoneyForGift(company, giftDto)) {
            throw new TooExpensiveGiftException(company.getName(), company.getBalance(), giftDto.amount());
        }
        company.setBalance(company.getBalance().subtract(giftDto.amount()));

        User user = userService.findUser(giftDto.userEmail());
        Gift gift = Gift.builder()
                .type(giftType)
                .amount(giftDto.amount())
                .expiredDate(getGiftExpiredDate(giftType, LocalDate.now()))
                .user(user)
                .build();

        giftRepository.save(gift);
        companyService.updateCompany(company);
    }

    private boolean hasEnoughMoneyForGift(Company company, GiftDto giftDto) {
        return (company.getBalance().compareTo(giftDto.amount()) >= 0);
    }

    public LocalDate getGiftExpiredDate(GiftType giftType, LocalDate receivedDate) {
        requireGiftType(giftType);

        LocalDate expiredDate;
        if (giftType == GiftType.CARD) {
            expiredDate = receivedDate.plusDays(giftCardExpiresInDays);
        } else {
            int nextYear = (receivedDate.getYear() + 1);
            YearMonth februaryNextYear = YearMonth.of(nextYear, Month.FEBRUARY);

            expiredDate = februaryNextYear.atEndOfMonth();
        }
        return expiredDate;
    }

    private void requireGiftType(GiftType giftType) {
        Objects.requireNonNull(giftType, "Le type de cadeau doit être obligatoirement renseigné");
    }

}
