package com.wedoogift.backend.exception;

import java.io.Serial;

public final class UserNotFoundException extends Exception {

    @Serial
    private static final long serialVersionUID = 3536803066255937252L;

    private static final String MESSAGE = "L'adresse mail '%s' n'est associée à aucun utilisateur Wedoogift";

    public UserNotFoundException(String userEmail) {
        super(MESSAGE.formatted(userEmail));
    }

}
