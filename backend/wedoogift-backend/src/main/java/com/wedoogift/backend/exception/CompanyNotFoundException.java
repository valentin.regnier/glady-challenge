package com.wedoogift.backend.exception;

import java.io.Serial;

public final class CompanyNotFoundException extends Exception {

    @Serial
    private static final long serialVersionUID = -5776283418364841272L;

    private static final String MESSAGE = "L'entreprise '%s' n'est pas connu par Wedoogift";

    public CompanyNotFoundException(String companyName) {
        super(MESSAGE.formatted(companyName));
    }

}
