package com.wedoogift.backend.service;

import com.wedoogift.backend.exception.CompanyNotFoundException;
import com.wedoogift.backend.model.entity.Company;
import com.wedoogift.backend.model.repository.CompanyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CompanyServiceDefault implements CompanyService {

    private final CompanyRepository companyRepository;

    @Override
    public Company findCompany(String companyName) throws CompanyNotFoundException {
        return companyRepository.findByName(companyName)
                .orElseThrow(() -> new CompanyNotFoundException(companyName));
    }

    @Override
    public void updateCompany(Company company) {
        companyRepository.save(company);
    }

}
