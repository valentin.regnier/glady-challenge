package com.wedoogift.backend.service;

import com.wedoogift.backend.exception.UserNotFoundException;
import com.wedoogift.backend.model.entity.User;

import java.math.BigDecimal;

public interface UserService {

    BigDecimal getUserBalance(String userEmail) throws UserNotFoundException;

    User findUser(String userEmail) throws UserNotFoundException;

}
