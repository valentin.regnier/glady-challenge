package com.wedoogift.backend.model.entity;

public enum GiftType {
    CARD,
    MEAL
}
