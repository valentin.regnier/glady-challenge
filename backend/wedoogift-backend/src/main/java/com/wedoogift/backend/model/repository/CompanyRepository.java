package com.wedoogift.backend.model.repository;

import com.wedoogift.backend.model.entity.Company;
import org.springframework.data.repository.ListCrudRepository;

import java.util.Optional;

public interface CompanyRepository extends ListCrudRepository<Company, Long> {

    Optional<Company> findByName(String name);

}
