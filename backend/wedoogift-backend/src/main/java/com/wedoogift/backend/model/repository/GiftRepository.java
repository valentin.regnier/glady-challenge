package com.wedoogift.backend.model.repository;

import com.wedoogift.backend.model.entity.Gift;
import org.springframework.data.repository.ListCrudRepository;

public interface GiftRepository extends ListCrudRepository<Gift, Long> {

}
