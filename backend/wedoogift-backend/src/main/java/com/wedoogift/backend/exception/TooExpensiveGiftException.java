package com.wedoogift.backend.exception;

import java.io.Serial;
import java.math.BigDecimal;

public final class TooExpensiveGiftException extends Exception {

    @Serial
    private static final long serialVersionUID = 3344626444860089813L;

    private static final String MESSAGE = "Le solde actuel (%s€) de l'entreprise %s n'est pas suffisant pour offrir un cadeau de %s€";

    public TooExpensiveGiftException(String companyName, BigDecimal companyBalance, BigDecimal giftAmount) {
        super(MESSAGE.formatted(companyBalance, companyName, giftAmount));
    }

}
