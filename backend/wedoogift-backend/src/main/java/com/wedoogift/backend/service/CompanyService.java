package com.wedoogift.backend.service;

import com.wedoogift.backend.exception.CompanyNotFoundException;
import com.wedoogift.backend.model.entity.Company;

public interface CompanyService {

    Company findCompany(String companyName) throws CompanyNotFoundException;

    void updateCompany(Company company);

}
