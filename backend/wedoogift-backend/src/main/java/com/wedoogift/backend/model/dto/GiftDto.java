package com.wedoogift.backend.model.dto;

import java.math.BigDecimal;

/**
 * Représente un cadeau envoyé par une entreprise à un utilisateur.
 *
 * @param companyName le nom de l'entreprise qui envoie le cadeau
 * @param userEmail   l'adresse mail de l'utilisateur qui reçoit le cadeau
 * @param amount      le montant du cadeau
 */
public record GiftDto(String companyName, String userEmail, BigDecimal amount) {

}
