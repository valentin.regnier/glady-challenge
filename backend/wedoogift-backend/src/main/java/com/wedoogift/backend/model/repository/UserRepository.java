package com.wedoogift.backend.model.repository;

import com.wedoogift.backend.model.entity.User;
import org.springframework.data.repository.ListCrudRepository;

import java.util.Optional;

public interface UserRepository extends ListCrudRepository<User, Long> {

    Optional<User> findByEmail(String email);

}
