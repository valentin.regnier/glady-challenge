package com.wedoogift.backend.service;

import com.wedoogift.backend.exception.CompanyNotFoundException;
import com.wedoogift.backend.exception.TooExpensiveGiftException;
import com.wedoogift.backend.exception.UserNotFoundException;
import com.wedoogift.backend.model.dto.GiftDto;
import com.wedoogift.backend.model.entity.GiftType;

public interface GiftService {

    void sendGift(GiftDto giftDto, GiftType giftType) throws CompanyNotFoundException, UserNotFoundException,
            TooExpensiveGiftException;


}
