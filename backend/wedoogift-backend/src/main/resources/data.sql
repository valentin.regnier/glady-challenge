INSERT INTO COMPANY (ID, NAME, BALANCE)
VALUES (1, 'Tesla', 500),
       (2, 'Apple', 100),
       (3, 'Oracle', 25),
       (4, 'Discord', 0);

INSERT INTO WEDOOGIFT_USER (ID, FIRST_NAME, LAST_NAME, EMAIL)
VALUES (1, 'Jean', 'Yves', 'jean.yves@test.com');
