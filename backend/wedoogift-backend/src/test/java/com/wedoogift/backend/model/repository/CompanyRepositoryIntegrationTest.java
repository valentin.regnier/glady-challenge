package com.wedoogift.backend.model.repository;

import com.wedoogift.backend.model.entity.Company;
import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("it")
class CompanyRepositoryIntegrationTest {

    @Autowired
    private CompanyRepository companyRepository;

    @Test
    void findByName() {
        String companyName = "Google";
        Company company = Company.builder().name(companyName).balance(BigDecimal.valueOf(1_000_000)).build();
        companyRepository.save(company);

        Optional<Company> companyOptional = companyRepository.findByName(companyName);

        assertThat(companyOptional)
                .get()
                .extracting(Company::getName, InstanceOfAssertFactories.STRING)
                .isEqualTo(companyName);
    }

    @Test
    void findByName_WithoutResult() {
        Optional<Company> companyOptional = companyRepository.findByName("contact@wedoogift.com");

        assertThat(companyOptional)
                .isEmpty();
    }

}