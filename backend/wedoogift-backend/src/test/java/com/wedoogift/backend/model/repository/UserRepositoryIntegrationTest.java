package com.wedoogift.backend.model.repository;

import com.wedoogift.backend.model.entity.User;
import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.assertj.core.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("it")
class UserRepositoryIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    void findByEmail() {
        String userEmail = "jean.yves@test.com";
        User user = User.builder().firstName("Jean").lastName("Yves").email(userEmail).build();
        userRepository.save(user);

        Optional<User> userOptional = userRepository.findByEmail(userEmail);

        assertThat(userOptional)
                .get()
                .extracting(User::getEmail, InstanceOfAssertFactories.STRING)
                .isEqualTo(userEmail);
    }

    @Test
    void findByEmail_WithoutResult() {
        Optional<User> userOptional = userRepository.findByEmail("yves.jean@test.com");

        assertThat(userOptional)
                .isEmpty();
    }

}