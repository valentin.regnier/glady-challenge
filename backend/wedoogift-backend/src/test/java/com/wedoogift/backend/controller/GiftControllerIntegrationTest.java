package com.wedoogift.backend.controller;

import com.wedoogift.backend.model.entity.Company;
import com.wedoogift.backend.model.entity.User;
import com.wedoogift.backend.model.repository.CompanyRepository;
import com.wedoogift.backend.model.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("it")
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class GiftControllerIntegrationTest {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        Company company = Company.builder().name("Asus").balance(BigDecimal.TEN).build();
        companyRepository.save(company);

        User user = User.builder().firstName("Jean").lastName("Yves").email("jean.yves@test.com").build();
        userRepository.save(user);
    }

    @ParameterizedTest
    @ValueSource(strings = {"/gift/card", "/gift/meal"})
    void sendGift_NoContent(String giftUrl) throws Exception {
        String body = """
                {
                  "companyName": "Asus",
                  "userEmail": "jean.yves@test.com",
                  "amount": 10
                }
                """;

        mockMvc.perform(post(giftUrl).contentType(MediaType.APPLICATION_JSON).content(body))
                .andExpect(status().isNoContent());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/gift/card", "/gift/meal"})
    void sendGift_UserNotFound(String giftUrl) throws Exception {
        String body = """
                {
                  "companyName": "Asus",
                  "userEmail": "test@test.com",
                  "amount": 10
                }
                """;

        mockMvc.perform(post(giftUrl).contentType(MediaType.APPLICATION_JSON).content(body))
                .andExpect(status().isNotFound())
                .andExpect(content().string(
                        "L'adresse mail 'test@test.com' n'est associée à aucun utilisateur Wedoogift"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/gift/card", "/gift/meal"})
    void sendGift_CompanyNotFound(String giftUrl) throws Exception {
        String body = """
                {
                  "companyName": "MSI",
                  "userEmail": "jean.yves@test.com",
                  "amount": 10
                }
                """;

        mockMvc.perform(post(giftUrl).contentType(MediaType.APPLICATION_JSON).content(body))
                .andExpect(status().isNotFound())
                .andExpect(content().string(
                        "L'entreprise 'MSI' n'est pas connu par Wedoogift"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/gift/card", "/gift/meal"})
    void sendGift_Conflict(String giftUrl) throws Exception {
        String body = """
                {
                  "companyName": "Asus",
                  "userEmail": "jean.yves@test.com",
                  "amount": 50
                }
                """;

        mockMvc.perform(post(giftUrl).contentType(MediaType.APPLICATION_JSON).content(body))
                .andExpect(status().isConflict())
                .andExpect(content().string(
                        "Le solde actuel (10.00€) de l'entreprise Asus n'est pas suffisant pour offrir un cadeau de 50€"));
    }

}