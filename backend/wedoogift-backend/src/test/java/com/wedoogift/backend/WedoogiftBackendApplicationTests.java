package com.wedoogift.backend;

import com.wedoogift.backend.controller.GiftController;
import com.wedoogift.backend.controller.UserController;
import com.wedoogift.backend.model.repository.CompanyRepository;
import com.wedoogift.backend.model.repository.GiftRepository;
import com.wedoogift.backend.model.repository.UserRepository;
import com.wedoogift.backend.service.CompanyService;
import com.wedoogift.backend.service.GiftService;
import com.wedoogift.backend.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.*;

@SpringBootTest
class WedoogiftBackendApplicationTests {

	@Value("${wedoogift.gift.card.expires-in-days}")
	private String cardGiftExpiresInDays;

	@Autowired
	private GiftController giftController;
	@Autowired
	private UserController userController;

	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private GiftRepository giftRepository;
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CompanyService companyService;
	@Autowired
	private GiftService giftService;
	@Autowired
	private UserService userService;

	@Test
	void contextLoads() {
		assertIsNotNull(giftController);
		assertIsNotNull(userController);

		assertIsNotNull(companyRepository);
		assertIsNotNull(giftRepository);
		assertIsNotNull(userRepository);

		assertIsNotNull(companyService);
		assertIsNotNull(giftService);
		assertIsNotNull(userService);
	}

	private void assertIsNotNull(Object o) {
		assertThat(o).isNotNull();
	}

	@Test
	void propertiesLoads() {
		assertThat(cardGiftExpiresInDays)
				.isNotBlank();
	}

}
