package com.wedoogift.backend.service;

import com.wedoogift.backend.exception.CompanyNotFoundException;
import com.wedoogift.backend.exception.TooExpensiveGiftException;
import com.wedoogift.backend.exception.UserNotFoundException;
import com.wedoogift.backend.model.dto.GiftDto;
import com.wedoogift.backend.model.entity.Company;
import com.wedoogift.backend.model.entity.Gift;
import com.wedoogift.backend.model.entity.GiftType;
import com.wedoogift.backend.model.entity.User;
import com.wedoogift.backend.model.repository.GiftRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.YearMonth;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GiftServiceDefaultTest {

    private static final int GIFT_CARD_EXPIRES_IN_DAYS = 365;

    private GiftServiceDefault giftService;

    @Mock
    private UserService userService;
    @Mock
    private CompanyService companyService;
    @Mock
    private GiftRepository giftRepository;

    @BeforeEach
    void setUp() {
        giftService = new GiftServiceDefault(GIFT_CARD_EXPIRES_IN_DAYS, userService, companyService, giftRepository);
    }

    @Nested
    class SendGift {

        @Captor
        private ArgumentCaptor<Gift> giftCaptor;

        @Captor
        private ArgumentCaptor<Company> companyCaptor;

        @Test
        @DisplayName("Une carte cadeau expire 365 jours après l'avoir envoyée")
        void sendGift_Card() throws UserNotFoundException, TooExpensiveGiftException, CompanyNotFoundException {
            Company company = Company.builder().id(7L).name("Fnac").balance(BigDecimal.valueOf(25)).build();
            User user = User.builder().id(42L).firstName("Jean").lastName("Yves").email("jean.yves@test.com").build();
            Gift gift = Gift.builder()
                    .type(GiftType.CARD)
                    .amount(BigDecimal.valueOf(25))
                    .expiredDate(LocalDate.now().plusDays(GIFT_CARD_EXPIRES_IN_DAYS))
                    .user(user)
                    .build();
            GiftDto giftDto = new GiftDto("Fnac", "jean.yves@test.com", BigDecimal.valueOf(25));

            when(companyService.findCompany("Fnac")).thenReturn(company);
            when(userService.findUser("jean.yves@test.com")).thenReturn(user);

            giftService.sendGift(giftDto, GiftType.CARD);

            verifyAndAssertGiftWithCaptor(gift);

            company.setBalance(BigDecimal.ZERO);
            verifyAndAssertCompanyWithCaptor(company);
        }

        @Test
        @DisplayName(
                "Un cadeau de nourriture expire le dernier jour de février de l'année suivante après l'avoir envoyée"
        )
        void sendGift_Meal() throws UserNotFoundException, TooExpensiveGiftException, CompanyNotFoundException {
            Company company = Company.builder().id(17L).name("Biocoop").balance(BigDecimal.valueOf(30)).build();
            User user = User.builder()
                    .id(24L)
                    .firstName("Jean")
                    .lastName("Dupond")
                    .email("jean.dupond@test.com")
                    .build();
            Gift gift = Gift.builder()
                    .type(GiftType.MEAL)
                    .amount(BigDecimal.valueOf(20))
                    .expiredDate(getLastDateOfNextFebruaryYear())
                    .user(user)
                    .build();
            GiftDto giftDto = new GiftDto("Biocoop", "jean.dupond@test.com", BigDecimal.valueOf(20));

            when(companyService.findCompany("Biocoop")).thenReturn(company);
            when(userService.findUser("jean.dupond@test.com")).thenReturn(user);

            giftService.sendGift(giftDto, GiftType.MEAL);

            verifyAndAssertGiftWithCaptor(gift);

            company.setBalance(BigDecimal.TEN);
            verifyAndAssertCompanyWithCaptor(company);
        }

        private LocalDate getLastDateOfNextFebruaryYear() {
            int nextYear = Year.now().plusYears(1).getValue();
            return YearMonth.of(nextYear, Month.FEBRUARY).atEndOfMonth();
        }

        private void verifyAndAssertGiftWithCaptor(Gift gift) {
            verify(giftRepository).save(giftCaptor.capture());

            assertThat(giftCaptor.getValue())
                    .usingRecursiveComparison()
                    .isEqualTo(gift);
        }

        private void verifyAndAssertCompanyWithCaptor(Company company) {
            verify(companyService).updateCompany(companyCaptor.capture());

            assertThat(companyCaptor.getValue())
                    .usingRecursiveComparison()
                    .isEqualTo(company);
        }

        @ParameterizedTest
        @EnumSource(value = GiftType.class)
        void sendGift_CompanyNotFoundException(GiftType giftType) throws CompanyNotFoundException {
            String companyName = "Darty";
            CompanyNotFoundException exception = new CompanyNotFoundException(companyName);
            GiftDto giftDto = new GiftDto(companyName, "jeannette.yves@test.com", BigDecimal.TEN);

            when(companyService.findCompany(companyName)).thenThrow(exception);

            assertThatThrownBy(() -> giftService.sendGift(giftDto, giftType))
                    .isEqualTo(exception);

            verifyGiftNeverSaved();
        }

        @ParameterizedTest
        @EnumSource(value = GiftType.class)
        void sendGift_UserNotFoundException(GiftType giftType) throws UserNotFoundException, CompanyNotFoundException {
            String companyName = "Top Achat";
            String userEmail = "yves.jean@test.com";
            UserNotFoundException exception = new UserNotFoundException(userEmail);
            Company company = Company.builder().id(95L).name(companyName).balance(BigDecimal.TEN).build();
            GiftDto giftDto = new GiftDto(companyName, userEmail, BigDecimal.TWO);

            when(companyService.findCompany(companyName)).thenReturn(company);
            when(userService.findUser(userEmail)).thenThrow(exception);

            assertThatThrownBy(() -> giftService.sendGift(giftDto, giftType))
                    .isEqualTo(exception);

            verifyGiftNeverSaved();
        }

        @ParameterizedTest
        @CsvSource({"CARD,25.01", "MEAL,100"})
        void sendGift_TooExpensiveGiftException(GiftType giftType, double giftAmount) throws CompanyNotFoundException {
            String companyName = "LDLC";
            BigDecimal companyBalance = BigDecimal.valueOf(25);
            Company company = Company.builder().id(59L).name(companyName).balance(companyBalance).build();
            GiftDto giftDto = new GiftDto(companyName, "jean@gmail.com", BigDecimal.valueOf(giftAmount));

            when(companyService.findCompany(companyName)).thenReturn(company);

            assertThatThrownBy(() -> giftService.sendGift(giftDto, giftType))
                    .isInstanceOf(TooExpensiveGiftException.class)
                    .hasMessage(
                            "Le solde actuel (%s€) de l'entreprise %s n'est pas suffisant pour offrir un cadeau de %s€",
                            companyBalance, companyName, giftAmount);

            verifyGiftNeverSaved();
        }

        @ParameterizedTest
        @EnumSource(value = GiftType.class)
        void sendGift_NullGiftDto(GiftType giftType) {
            assertThatNullPointerException()
                    .isThrownBy(() -> giftService.sendGift(null, giftType));

            verifyGiftNeverSaved();
        }

        @Test
        void sendGift_NullGiftType() {
            GiftDto giftDto = new GiftDto("Amazon", "dupont@test.com", BigDecimal.TEN);

            assertThatNullPointerException()
                    .isThrownBy(() -> giftService.sendGift(giftDto, null))
                    .withMessage("Le type de cadeau doit être obligatoirement renseigné");
        }

        private void verifyGiftNeverSaved() {
            verify(companyService, never()).updateCompany(any());
            verify(giftRepository, never()).save(any());
        }

    }

    @Test
    void getGiftExpiredDate_Card() {
        LocalDate receivedDate = LocalDate.of(2022, 5, 25);

        LocalDate expiredDate = giftService.getGiftExpiredDate(GiftType.CARD, receivedDate);

        assertThat(expiredDate)
                .isEqualTo(LocalDate.of(2023, 5, 25));
    }

    @Test
    void getGiftExpiredDate_CardInLeapYear() {
        LocalDate receivedDate = LocalDate.of(2023, 9, 24);

        LocalDate expiredDate = giftService.getGiftExpiredDate(GiftType.CARD, receivedDate);

        assertThat(expiredDate)
                .isEqualTo(LocalDate.of(2024, 9, 23));
    }

    @Test
    void getGiftExpiredDate_Meal() {
        LocalDate receivedDate = LocalDate.of(2022, 1, 18);

        LocalDate expiredDate = giftService.getGiftExpiredDate(GiftType.MEAL, receivedDate);

        assertThat(expiredDate)
                .isEqualTo(LocalDate.of(2023, 2, 28));
    }

    @Test
    void getGiftExpiredDate_MealInLeapYear() {
        LocalDate receivedDate = LocalDate.of(2023, 10, 14);

        LocalDate expiredDate = giftService.getGiftExpiredDate(GiftType.MEAL, receivedDate);

        assertThat(expiredDate)
                .isEqualTo(LocalDate.of(2024, 2, 29));
    }

    @Test
    void getGiftExpiredDate_NullGiftType() {
        assertThatNullPointerException()
                .isThrownBy(() -> giftService.getGiftExpiredDate(null, LocalDate.now()))
                .withMessage("Le type de cadeau doit être obligatoirement renseigné");
    }

    @ParameterizedTest
    @EnumSource(value = GiftType.class)
    void getGiftExpiredDate_NullReceivedDate(GiftType giftType) {
        assertThatNullPointerException()
                .isThrownBy(() -> giftService.getGiftExpiredDate(giftType, null));
    }

}