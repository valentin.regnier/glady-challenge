package com.wedoogift.backend.service;

import com.wedoogift.backend.exception.UserNotFoundException;
import com.wedoogift.backend.model.entity.Gift;
import com.wedoogift.backend.model.entity.GiftType;
import com.wedoogift.backend.model.entity.User;
import com.wedoogift.backend.model.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceDefaultTest {

    @InjectMocks
    private UserServiceDefault userService;

    @Mock
    private UserRepository userRepository;

    @Test
    void getUserBalance() throws UserNotFoundException {
        LocalDate now = LocalDate.now();
        List<Gift> giftList = List.of(
                Gift.builder().type(GiftType.CARD).amount(BigDecimal.valueOf(50)).expiredDate(now).build(),
                Gift.builder()
                        .type(GiftType.CARD)
                        .amount(BigDecimal.valueOf(150))
                        .expiredDate(now.minusDays(1)) // Expiré
                        .build(),
                Gift.builder()
                        .type(GiftType.MEAL)
                        .amount(BigDecimal.valueOf(99.99))
                        .expiredDate(now.plusMonths(5))
                        .build(),
                Gift.builder()
                        .type(GiftType.MEAL)
                        .amount(BigDecimal.valueOf(79.99))
                        .expiredDate(now.minusMonths(9)) // Expiré
                        .build()
        );
        String userEmail = "jean.yves@test.com";
        User user = User.builder().email(userEmail).gifts(giftList).build();
        when(userRepository.findByEmail(userEmail)).thenReturn(Optional.of(user));

        BigDecimal userBalance = userService.getUserBalance(userEmail);

        assertThat(userBalance)
                .isEqualTo(BigDecimal.valueOf(149.99));
    }

    @Test
    void getUserBalance_WithAllGiftExpired() throws UserNotFoundException {
        LocalDate now = LocalDate.now();
        List<Gift> giftList = List.of(
                Gift.builder().type(GiftType.CARD).amount(BigDecimal.valueOf(37)).expiredDate(now.minusDays(1)).build(),
                Gift.builder()
                        .type(GiftType.MEAL)
                        .amount(BigDecimal.valueOf(50.99))
                        .expiredDate(now.minusMonths(1))
                        .build(),
                Gift.builder()
                        .type(GiftType.CARD)
                        .amount(BigDecimal.valueOf(11))
                        .expiredDate(now.minusYears(1))
                        .build()
        );
        String userEmail = "test@test.com";
        User user = User.builder().email(userEmail).gifts(giftList).build();
        when(userRepository.findByEmail(userEmail)).thenReturn(Optional.of(user));

        BigDecimal userBalance = userService.getUserBalance(userEmail);

        assertThat(userBalance)
                .isZero();
    }

    @Test
    void getUserBalance_WithoutGift() throws UserNotFoundException {
        String userEmail = "yves.jean@test.com";
        User user = User.builder().email("jean.yves@test.com").build();
        when(userRepository.findByEmail(userEmail)).thenReturn(Optional.of(user));

        BigDecimal userBalance = userService.getUserBalance(userEmail);

        assertThat(userBalance)
                .isZero();
    }

    @Test
    void getUserBalance_UserNotFoundException() {
        String userEmail = "jeannette.yves@test.com";
        when(userRepository.findByEmail(userEmail)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> userService.getUserBalance(userEmail))
                .isInstanceOf(UserNotFoundException.class)
                .hasMessage("L'adresse mail '%s' n'est associée à aucun utilisateur Wedoogift", userEmail);
    }

    @Test
    void findUser() throws UserNotFoundException {
        String email = "jean.yves@test.com";
        User user = User.builder().id(68L).firstName("Jean").lastName("Yves").email(email).build();
        when(userRepository.findByEmail(email)).thenReturn(Optional.of(user));

        User userFound = userService.findUser(email);

        assertThat(userFound)
                .usingRecursiveComparison()
                .isEqualTo(user);
    }

    @Test
    void findUser_UserNotFoundException() {
        String email = "yves.jean@test.com";
        when(userRepository.findByEmail(email)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> userService.findUser(email))
                .isInstanceOf(UserNotFoundException.class)
                .hasMessage("L'adresse mail '%s' n'est associée à aucun utilisateur Wedoogift", email);
    }

}