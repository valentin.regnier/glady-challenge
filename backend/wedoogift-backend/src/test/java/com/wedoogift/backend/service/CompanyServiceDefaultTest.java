package com.wedoogift.backend.service;

import com.wedoogift.backend.exception.CompanyNotFoundException;
import com.wedoogift.backend.model.entity.Company;
import com.wedoogift.backend.model.repository.CompanyRepository;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CompanyServiceDefaultTest {

    @InjectMocks
    private CompanyServiceDefault companyService;

    @Mock
    private CompanyRepository companyRepository;

    @Test
    void findCompany() throws CompanyNotFoundException {
        String companyName = "LDLC";
        Company company = Company.builder().id(87L).name(companyName).balance(BigDecimal.valueOf(957)).build();
        when(companyRepository.findByName(companyName)).thenReturn(Optional.of(company));

        Company companyFound = companyService.findCompany(companyName);

        assertThat(companyFound)
                .usingRecursiveComparison()
                .isEqualTo(company);
    }

    @Test
    void findCompany_CompanyNotFoundException() {
        String companyName = "Top Achat";
        when(companyRepository.findByName(companyName)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> companyService.findCompany(companyName))
                .isInstanceOf(CompanyNotFoundException.class)
                .hasMessage("L'entreprise '%s' n'est pas connu par Wedoogift", companyName);
    }

    @Nested
    class UpdateCompany {

        @Captor
        private ArgumentCaptor<Company> companyCaptor;

        @Test
        void updateCompany() {
            Company company = Company.builder().id(25L).name("MSI").balance(BigDecimal.TEN).build();

            companyService.updateCompany(company);

            verify(companyRepository).save(companyCaptor.capture());
            assertThat(companyCaptor.getValue())
                    .usingRecursiveComparison()
                    .isEqualTo(company);
        }

    }

}