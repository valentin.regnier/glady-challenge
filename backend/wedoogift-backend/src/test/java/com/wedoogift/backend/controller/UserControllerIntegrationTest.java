package com.wedoogift.backend.controller;

import com.wedoogift.backend.model.entity.Gift;
import com.wedoogift.backend.model.entity.GiftType;
import com.wedoogift.backend.model.entity.User;
import com.wedoogift.backend.model.repository.GiftRepository;
import com.wedoogift.backend.model.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("it")
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class UserControllerIntegrationTest {

    private static final String URL = "/user/{userEmail}/balance";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GiftRepository giftRepository;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        User user = User.builder()
                .firstName("Jean")
                .lastName("Yves")
                .email("jean.yves@test.com")
                .build();
        userRepository.save(user);

        Gift gift = Gift.builder()
                .type(GiftType.CARD)
                .amount(new BigDecimal("10.00"))
                .expiredDate(LocalDate.now().plusMonths(1))
                .user(user)
                .build();
        giftRepository.save(gift);
    }

    @Test
    void getBalance_Ok() throws Exception {
        mockMvc.perform(get(URL, "jean.yves@test.com"))
                .andExpect(status().isOk())
                .andExpect(content().string("$10"));
    }

    @Test
    void getBalance_NotFound() throws Exception {
        mockMvc.perform(get(URL, "test@test.com"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(
                        "L'adresse mail 'test@test.com' n'est associée à aucun utilisateur Wedoogift"));
    }

}