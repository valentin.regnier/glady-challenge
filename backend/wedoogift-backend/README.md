# Wedoogift Backend

API Rest développé avec Spring Boot et Java 21.

## Test

Commande pour lancer l'ensemble des tests du projet :

`mvn test`

## Run

Commande pour démarrer l'application :

`mvn clean install spring-boot:run`

## API

Une fois l'application démarrée, il est possible de trouver une documentation de l'API via cette url :

http://localhost:8080/api

## Données

Voici les données initialisées lors du lancement de l'application :

### Company

- Tesla, $500
- Apple, $200
- Oracle, $25
- Discord, $0

### User

- Jean Yves, jean.yves@test.com

### Ajouter des données

Si besoin, il est possible d'ajouter des données supplémentaires via la console H2:

1. Lancer l'application avec la commande `mvn spring-boot:run -Dspring-boot.run.profiles=dev`
2. Accéder à l'URL suivante via un navigateur : http://localhost:8080/h2-console
3. Renseigner les valeurs suivantes (mot de passe vide) : url = jdbc:h2:mem:wedoogiftDB; user = sa

## Maven Wrapper

Attention ! Le wrapper Maven du projet ne fonctionne pas car il ne prend pas en charge Java 21.

Merci d'utiliser une installation Maven liée à un jdk 21.

